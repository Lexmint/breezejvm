package com.guybydefault.parser;

/**
 * Created: 02/12/17 (14:46).
 */
public class TextRange {
    private int sourceFileIndex;
    private int position;
    private int length;
    private int line;
    private int column;

    TextRange(int sourceFileIndex, int position, int length, int line, int column) {
        this.sourceFileIndex = sourceFileIndex;
        this.position = position;
        this.length = length;
        this.line = line;
        this.column = column;
    }

    public int getSourceFileIndex() {
        return sourceFileIndex;
    }

    public int getPosition() {
        return position;
    }

    public int getLength() {
        return length;
    }

    public int getLine() {
        return line;
    }

    public int getColumn() {
        return column;
    }
}
