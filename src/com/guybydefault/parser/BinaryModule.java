package com.guybydefault.parser;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created: 30/11/17 (15:11).
 */
public class BinaryModule {

    private ModuleHeader moduleHeader;
    private TableHeader tableHeader;
    private Section[] sections;
    private Symbol[] symbols;
    private SourceFile[] sourceFiles;
    private TextRange[] textRanges;
    private CodePoint[] codePoints;
    private byte[][] blobs;
    private String[] strings;


    public BinaryModule(byte[] content) {
        ByteIterator bIt = new ByteIterator(content);
        byte[] signature = bIt.getNextByteArray(0xC);
        int formatVersion = bIt.getNextInt();
        int platformNameIndex = bIt.getNextInt();
        int platformVersion = bIt.getNextInt();
        long entryPoint = bIt.getNextLong();
        this.moduleHeader = new ModuleHeader(signature, formatVersion, platformNameIndex, platformVersion, entryPoint);

        int sectionsCount = bIt.getNextInt();
        int symbolsCount = bIt.getNextInt();
        int sourceFilesCount = bIt.getNextInt();
        int sourceTextRangesCount = bIt.getNextInt();
        int sourceCodePointsCount = bIt.getNextInt();
        int blobsCount = bIt.getNextInt();
        int stringsCount = bIt.getNextInt();
        this.tableHeader = new TableHeader(sectionsCount, symbolsCount, sourceFilesCount, sourceTextRangesCount, sourceCodePointsCount, blobsCount, stringsCount);

        this.sections = new Section[sectionsCount];
        for (int i = 0; i < sectionsCount; i++) {
            int blobIndex = bIt.getNextInt();
            int bankNameIndex = bIt.getNextInt();
            long startAddress = bIt.getNextLong();
            SectionKind kind = SectionKind.getSectionKindByCode(bIt.getNextShort());
            int customSectionNameIndex = bIt.getNextInt();
            SectionAccessMode accessMode = SectionAccessMode.getSectionAccessModeByCode(bIt.getNextShort());
            sections[i] = new Section(i, blobIndex, bankNameIndex, startAddress, kind, customSectionNameIndex, accessMode);
        }

        this.symbols = new Symbol[symbolsCount];
        for (int i = 0; i < symbolsCount; i++) {
            int sectionIndex = bIt.getNextInt();
            long blobEntryIndex = bIt.getNextLong();
            int nameIndex = bIt.getNextInt();
            symbols[i] = new Symbol(sectionIndex, blobEntryIndex, nameIndex);
        }

        this.sourceFiles = new SourceFile[sourceFilesCount];
        for (int i = 0; i < sourceFilesCount; i++) {
            int fileNameIndex = bIt.getNextInt();
            int sha256hashBytesIndex = bIt.getNextInt();
            sourceFiles[i] = new SourceFile(fileNameIndex, sha256hashBytesIndex);
        }

        this.textRanges = new TextRange[sourceTextRangesCount];
        for (int i = 0; i < sourceTextRangesCount; i++) {
            int sourceFileIndex = bIt.getNextInt();
            int position = bIt.getNextInt();
            int length = bIt.getNextInt();
            int line = bIt.getNextInt();
            int column = bIt.getNextInt();
            textRanges[i] = new TextRange(sourceFileIndex, position, length, line, column);
        }
        this.codePoints = new CodePoint[sourceCodePointsCount];
        for (int i = 0; i < sourceCodePointsCount; i++) {
            long address = bIt.getNextLong();
            int sourceOperationRangeIndex = bIt.getNextInt();
            codePoints[i] = new CodePoint(address, sourceOperationRangeIndex);
        }

        this.blobs = new byte[blobsCount][];
        int[] blobsLength = new int[blobsCount];
        for (int i = 0; i < blobsCount; i++) {
            blobsLength[i] = bIt.getNextInt();
        }
        for (int i = 0; i < blobsCount; i++) {
            this.blobs[i] = bIt.getNextByteArray(blobsLength[i]);
        }
        this.strings = new String[stringsCount];
        for (int i = 0; i < stringsCount; i++) {
            int strLength = bIt.getNextInt();
            strings[i] = new String(bIt.getNextByteArray(strLength));
        }
        for (Section section : sections) {
            section.setSectionName(strings[section.getBankNameIndex()]);
        }
        for (int i = 0; i < symbolsCount; i++) {
            symbols[i].setName(strings[symbols[i].getNameIndex()]);
        }
    }


    public ModuleHeader getModuleHeader() {
        return moduleHeader;
    }

    public TableHeader getTableHeader() {
        return tableHeader;
    }

    public Section[] getSections() {
        return sections;
    }

    public Section getSectionByName(String name) {
        for (Section section : sections) {
            if (section.getSectionName().equals(name)) {
                return section;
            }
        }
        return null;
    }

    public Symbol[] getSymbols() {
        return symbols;
    }

    public SourceFile[] getSourceFiles() {
        return sourceFiles;
    }

    public TextRange[] getTextRanges() {
        return textRanges;
    }

    public CodePoint[] getCodePoints() {
        return codePoints;
    }

    public byte[][] getBlobs() {
        return blobs;
    }

    public String[] getStrings() {
        return strings;
    }

    public static BinaryModule importModule(String strPath) throws IOException {
        Path path = Paths.get(strPath);
        byte[] bytes = Files.readAllBytes(path);
        BinaryModule bm = new BinaryModule(bytes);
        return bm;
    }

    public int getOffsetByLabel(String label) {
        int i = 0;
        while (i < getStrings().length) {
            if (getStrings()[i].equals(label)) {
                int k = 0;
                while (k < getSymbols().length) {
                    if (getSymbols()[k].getNameIndex() == i) {
                        return (int) getSymbols()[k].getBlobEntryIndex();
                    }
                    k++;
                }
                throw new IllegalArgumentException("Symbol with label " + label + " not been found");
            }
            i++;
        }
        throw new IllegalArgumentException("String " + label + " has not been found");
    }
}
