package com.guybydefault.parser;

/**
 * Created: 02/12/17 (14:47).
 */
public class TableHeader {
    private int sectionsCount;
    private int symbolsCount;
    private int sourceFilesCount;
    private int sourceTextRangesCount;
    private int sourceCodePointsCount;
    private int blobsCount;
    private int stringsCount;

     TableHeader(int sectionsCount, int symbolsCount, int sourceFilesCount, int sourceTextRangesCount, int sourceCodePointsCount, int blobsCount, int stringsCount) {
        this.sectionsCount = sectionsCount;
        this.symbolsCount = symbolsCount;
        this.sourceFilesCount = sourceFilesCount;
        this.sourceTextRangesCount = sourceTextRangesCount;
        this.sourceCodePointsCount = sourceCodePointsCount;
        this.blobsCount = blobsCount;
        this.stringsCount = stringsCount;
    }

    public int getSectionsCount() {
        return sectionsCount;
    }

    public int getSymbolsCount() {
        return symbolsCount;
    }

    public int getSourceFilesCount() {
        return sourceFilesCount;
    }

    public int getSourceTextRangesCount() {
        return sourceTextRangesCount;
    }

    public int getSourceCodePointsCount() {
        return sourceCodePointsCount;
    }

    public int getBlobsCount() {
        return blobsCount;
    }

    public int getStringsCount() {
        return stringsCount;
    }
}