package com.guybydefault.parser;

/**
 * Created: 02/12/17 (14:47).
 */
public class CodePoint {
        private long address;
        private int sourceOperationRangeIndex;

         CodePoint(long address, int sourceOperationRangeIndex) {
            this.address = address;
            this.sourceOperationRangeIndex = sourceOperationRangeIndex;
        }

}
