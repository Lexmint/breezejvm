package com.guybydefault.parser;

/**
 * Created: 02/12/17 (14:47).
 */
public class Symbol {

        private int sectionIndex;
        private long blobEntryIndex;
        private int nameIndex;
        private String name;

        Symbol(int sectionIndex, long blobEntryIndex, int nameIndex) {
            this.sectionIndex = sectionIndex;
            this.blobEntryIndex = blobEntryIndex;
            this.nameIndex = nameIndex;
        }

        public int getSectionIndex() {
            return sectionIndex;
        }

        public long getBlobEntryIndex() {
            return blobEntryIndex;
        }

        public int getNameIndex() {
            return nameIndex;
        }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
