package com.guybydefault.parser;

/**
 * Created: 02/12/17 (14:48).
 */
public class ModuleHeader {
    private byte[] signature;
    private int formatVersion;
    private int platformNameIndex;
    private int platformVersion;
    private long entryPoint;

     ModuleHeader(byte[] signature, int formatVersion, int platformNameIndex, int platformVersion, long entryPoint) {
        this.signature = signature;
        this.formatVersion = formatVersion;
        this.platformNameIndex = platformNameIndex;
        this.platformVersion = platformVersion;
        this.entryPoint = entryPoint;
    }

    public byte[] getSignature() {
        return signature;
    }

    public int getFormatVersion() {
        return formatVersion;
    }

    public int getPlatformNameIndex() {
        return platformNameIndex;
    }

    public int getPlatformVersion() {
        return platformVersion;
    }

    public long getEntryPoint() {
        return entryPoint;
    }
}