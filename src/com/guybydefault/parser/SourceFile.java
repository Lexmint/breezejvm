package com.guybydefault.parser;

/**
 * Created: 02/12/17 (14:46).
 */
public class SourceFile {

        private int fileNameIndex;
        private int sha256hashBytesIndex;

         SourceFile(int fileNameIndex, int sha256hashBytesIndex) {
            this.fileNameIndex = fileNameIndex;
            this.sha256hashBytesIndex = sha256hashBytesIndex;
        }

}
