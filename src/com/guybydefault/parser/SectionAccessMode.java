package com.guybydefault.parser;

/**
 * Created: 02/12/17 (14:55).
 */
public enum SectionAccessMode {
    NONE((short) 0),
    READ((short) 1),
    WRITE((short) 2),
    EXECUTE((short) 4);

    private int code;

    private SectionAccessMode(short code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public static SectionAccessMode getSectionAccessModeByCode(short code) {
        for (SectionAccessMode accessMode : values()) {
            if (accessMode.getCode() == code) {
                return accessMode;
            }
        }
        throw new IllegalArgumentException(String.valueOf(code));
    }
}
