package com.guybydefault.parser;

/**
 * Created: 02/12/17 (14:47).
 */
public class Section {
    private int sectionId;
    private int blobIndex;
    private int bankNameIndex;
    private String sectionName;
    private long startAddress;
    private SectionKind kind;
    private int customSectionNameIndex;
    private SectionAccessMode accessMode;

    Section(int sectionId, int blobIndex, int bankNameIndex, long startAddress, SectionKind kind, int customSectionNameIndex, SectionAccessMode accessMode) {
        this.sectionId = sectionId;
        this.blobIndex = blobIndex;
        this.bankNameIndex = bankNameIndex;
        this.startAddress = startAddress;
        this.kind = kind;
        this.customSectionNameIndex = customSectionNameIndex;
        this.accessMode = accessMode;
    }


    public int getSectionId() {
        return sectionId;
    }

    public int getBlobIndex() {
        return blobIndex;
    }

    public int getBankNameIndex() {
        return bankNameIndex;
    }

    public long getStartAddress() {
        return startAddress;
    }

    public SectionKind getKind() {
        return kind;
    }

    public int getCustomSectionNameIndex() {
        return customSectionNameIndex;
    }

    public SectionAccessMode getAccessMode() {
        return accessMode;
    }

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }
}
