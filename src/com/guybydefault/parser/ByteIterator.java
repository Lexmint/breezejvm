package com.guybydefault.parser;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

/**
 * Created: 30/11/17 (17:53).
 */
public class ByteIterator {
    private byte[] bytes;
    private int currPos = 0;

    public ByteIterator(byte[] bytes) {
        this.bytes = bytes;
    }

    public int getCurrPos() {
        return currPos;
    }

    public void setCurrPos(int currPos) {
        this.currPos = currPos;
    }

    // OLD VERSION
//    private long getNextLong(int offset) {
//        long res = 0;
//        int i = 0;
//        while (i < offset) {
//            res <<= 8;
//            res |= bytes[currPos + i] & 0xFF;
//            i++;
//        }
//        currPos += i;
//        return res;
//    }

    public int getNextInt() {
        int res = ByteBuffer.wrap(bytes, currPos, 4).order(ByteOrder.LITTLE_ENDIAN).getInt();
        currPos += 4;
        return res;
    }

    public long getNextLong() {
        long res = ByteBuffer.wrap(bytes, currPos, 8).order(ByteOrder.LITTLE_ENDIAN).getLong();
        currPos += 8;
        return res;
    }

    public short getNextShort() {
        short res = ByteBuffer.wrap(bytes, currPos, 2).order(ByteOrder.LITTLE_ENDIAN).getShort();
        currPos += 2;
        return res;
    }

    public byte[] getNextByteArray(int count) {
        if (count == 0) {
            return new byte[0];
        }
        byte[] arr = Arrays.copyOfRange(bytes, currPos, currPos + count);
        currPos += count;
        return arr;
    }
}
