package com.guybydefault.parser;

/**
 * Created: 02/12/17 (18:55).
 */
public class Label {
    private long offset;
    private String name;
    private int sectionId;

    public Label(long offset, String name, int sectionId) {
        this.offset = offset;
        this.name = name;
        this.sectionId = sectionId;
    }
}
