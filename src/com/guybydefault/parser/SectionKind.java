package com.guybydefault.parser;

/**
 * Created: 02/12/17 (14:52).
 */
public enum SectionKind {
    CODE((short) 1),
    DATA((short) 2),
    CONST((short) 3),
    CUSTOM((short) 255);

    private short code;

    SectionKind(short code) {
        this.code = code;
    }

    public short getCode() {
        return code;
    }

    public static SectionKind getSectionKindByCode(short code) {
        for (SectionKind sectionKind : values()) {
            if (sectionKind.getCode() == code) {
                return sectionKind;
            }
        }
        return SectionKind.CUSTOM;
//        throw new IllegalArgumentException(String.valueOf(code));
    }
}
