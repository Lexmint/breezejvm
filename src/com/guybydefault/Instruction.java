package com.guybydefault;

/**
 * Created: 02/12/17 (19:52).
 */
public enum Instruction {
    nop(0x00, InstructionType.OTHER),
    ipush(0x10, InstructionType.OTHER),
    mipush(0x11, InstructionType.OTHER),
    iaload(0x2e, InstructionType.OTHER),
    istore(0x36, InstructionType.OTHER),
    iastore(0x4f, InstructionType.OTHER),
    pop(0x57, InstructionType.OTHER),
    dup(0x59, InstructionType.OTHER),
    dup2(0x5c, InstructionType.OTHER),

    iadd(0x60, InstructionType.OTHER),
    isub(0x64, InstructionType.OTHER),
    imul(0x68, InstructionType.OTHER),
    idiv(0x6c, InstructionType.OTHER),
    irem(0x70, InstructionType.OTHER),
    ineg(0x74, InstructionType.OTHER),

    ishl(0x78, InstructionType.OTHER),
    ishr(0x7a, InstructionType.OTHER),
    iushr(0x7c, InstructionType.OTHER),
    iand(0x7e, InstructionType.OTHER),
    ior(0x80, InstructionType.OTHER),
    ixor(0x82, InstructionType.OTHER),
    idec(0x83, InstructionType.OTHER),
    iinc(0x84, InstructionType.OTHER),

    ifeq(0x99, InstructionType.CONDITIONAL_JUMP),
    ifne(0x9a, InstructionType.CONDITIONAL_JUMP),
    iflt(0x9b, InstructionType.CONDITIONAL_JUMP),
    ifge(0x9c, InstructionType.CONDITIONAL_JUMP),
    ifgt(0x9d, InstructionType.CONDITIONAL_JUMP),
    ifle(0x9e, InstructionType.CONDITIONAL_JUMP),

    ificmpeq(0x9f, InstructionType.CONDITIONAL_JUMP),
    ificmpne(0xa0, InstructionType.CONDITIONAL_JUMP),
    ificmplt(0xa1, InstructionType.CONDITIONAL_JUMP),
    ificmpge(0xa2, InstructionType.CONDITIONAL_JUMP),
    ificmpgt(0xa3, InstructionType.CONDITIONAL_JUMP),
    ificmple(0xa4, InstructionType.CONDITIONAL_JUMP),

    jmp(0xa7, InstructionType.OTHER),
    invoke(0xb8, InstructionType.OTHER),
    ireturn(0xac, InstructionType.OTHER),
    ret(0xb1, InstructionType.OTHER),

    iread(0xc0, InstructionType.OTHER),
    iwrite(0xc1, InstructionType.OTHER);

    private int code;
    private InstructionType type;

    Instruction(int code, InstructionType type) {
        this.code = code;
        this.type = type;
    }

    public int getCode() {
        return code;
    }


    public static Instruction getInstructionByCode(int code) {
        for (Instruction instruction : values()) {
            if (instruction.getCode() == code) {
                return instruction;
            }
        }
        throw new IllegalArgumentException("Instruction with the code " + String.valueOf(code) + " does not exist.");

    }

    public InstructionType getType() {
        return type;
    }
}
