package com.guybydefault;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

/**
 * Created: 30/11/17 (15:20).
 */
public class Util {
    private final static char[] hexArray = "0123456789ABCDEF".toCharArray();

    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4]; // cut lowest 4 bits by doing logical shift
            hexChars[j * 2 + 1] = hexArray[v & 0x0F]; // cut highest 4 bits
        }
        return new String(hexChars);
    }

    public static int getIntFromBytesLittleEndian(byte[] bytes, int startIndex) {
        return ByteBuffer.wrap(bytes, startIndex, 4).order(ByteOrder.LITTLE_ENDIAN).getInt();
    }

    public static byte[] getBytesFromIntLittleEndian(int value) {
        return ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(value).array();
    }
}
