package com.guybydefault;

import java.util.Arrays;

/**
 * Created by lexmint on 13/12/2017.
 */
public class MemoryRegion {

    private byte[] region;

    public MemoryRegion(byte[] region) {
        this.region = region;
    }

    public void writeIntToMemory(int offset, int value) {
        System.arraycopy(Util.getBytesFromIntLittleEndian(value), 0, region, offset, Interpreter.INTEGER_SIZE);
    }

    public byte getByte(int offset) {
        return region[offset];
    }

    public int readInt(int offset) {
        return Util.getIntFromBytesLittleEndian(region, offset);
    }

    public Instruction readInstruction(int offset) {
        return Instruction.getInstructionByCode(0xff & region[offset]);
    }

    public int[] readArray(int offset, int numOfElements) {
        int[] arr = new int[numOfElements];

        for (int i = 0; i < numOfElements; i++) {
            arr[i] = readInt(offset + i * Interpreter.INTEGER_SIZE);
        }
        return arr;
    }

    public void printIntArray(int offset, int numOfElements) {
        System.out.println(Arrays.toString(readArray(offset, numOfElements)));
    }

    public int getSize() {
        return region.length;
    }

}
