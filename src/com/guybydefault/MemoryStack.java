package com.guybydefault;

/**
 * Created by lexmint on 13/12/2017.
 */
public class MemoryStack {

    private MemoryRegion memoryRegion;
    private int size;
    private int maxStackLength;
    private int startMemoryOffset;

    public MemoryStack(MemoryRegion memoryRegion, int startMemoryOffset, int maxStackLength) {
        this.memoryRegion = memoryRegion;
        this.startMemoryOffset = startMemoryOffset;
        this.size = 0;
        this.maxStackLength = maxStackLength;
    }

    private int getMemoryOffsetByIndex(int elementIndex) {
        return startMemoryOffset + elementIndex * Interpreter.INTEGER_SIZE;
    }

    private int getMemoryOffsetOfTop() {
        return getMemoryOffsetByIndex(size - 1);
    }

    public void push(int val) {
        if (size + 1 > maxStackLength) {
            throw new StackOverflowError("Stack is in trouble.");
        }
        memoryRegion.writeIntToMemory(getMemoryOffsetByIndex(size), val);
        size++;
    }

    public int pop() {
        int res = peek();
        size--;
        return res;
    }

    public int peek() {
        if (size == 0) {
            throw new UnsupportedOperationException("Stack is empty");
        }
        int res = memoryRegion.readInt(getMemoryOffsetOfTop());
        return res;
    }

    public void reverseLastElements(int numOfElements) {
        if (numOfElements < 0) {
            throw new UnsupportedOperationException("Wrong number of elements " + numOfElements);
        }
        if (numOfElements > size) {
            throw new UnsupportedOperationException("Stack does not contain " + numOfElements + " number of elements");
        }

        int r = size - 1;
        int l = size - numOfElements;
        while (l < r) {
            int tmp = memoryRegion.readInt(getMemoryOffsetByIndex(l));
            memoryRegion.writeIntToMemory(getMemoryOffsetByIndex(l), memoryRegion.readInt(getMemoryOffsetByIndex(r)));
            memoryRegion.writeIntToMemory(getMemoryOffsetByIndex(r), tmp);
            r--;
            l++;
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        int i = size - 1;
        while (i != -1) {
            sb.append(memoryRegion.readInt(getMemoryOffsetByIndex(i)));
            i--;
            if (i != -1) {
                sb.append(", ");
            }
        }
        return sb.toString();
    }
}
