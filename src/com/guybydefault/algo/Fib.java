package com.guybydefault.algo;

/**
 * Created: 03/12/17 (20:54).
 */
public class Fib {
    public static int fib(int n) {
        int i = 0;
        int a = 0;
        int b = 1;
        while (i < n) {
            int tmp = b;
            b = a + b;
            a = tmp;
            i++;
        }
        return a;
    }
    public static void main(String[] args) {
        System.out.println(fib(0xAF));
    }
}
