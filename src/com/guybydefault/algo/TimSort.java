package com.guybydefault.algo;

import java.util.Arrays;
import java.util.Random;

/**
 * Created: 03/12/17 (20:55).
 */
public class TimSort {

    public static void main(String[] args) {
        Random random = new Random();
        for (int i = 0; i < 3000; i++) {
            int a[] = new int[i];
            for (int k = 0; k < i; k++) {
                a[k] = random.nextInt(30);
            }
            int[] cl = a.clone();
            TimSort ts = new TimSort();
            int[] tsSorted = a.clone();
            ts.timSort(tsSorted);
            Arrays.sort(a);
            boolean res = Arrays.equals(a, tsSorted);
            if (!res) {
                System.out.println("Timsorted: " + Arrays.toString(tsSorted));
                System.out.println("JavaSorte: " + Arrays.toString(a));
                System.out.println("SourceArr: " + Arrays.toString(cl));
                break;
            }
        }
    }

    private int[] a;

    private final static int MAX_RUNS = 64 * 100;
    private int[] runsIndexes = new int[MAX_RUNS];
    private int[] runsSizes = new int[MAX_RUNS];
    private int[] stackIndexes = new int[MAX_RUNS];
    private int[] stackSizes = new int[MAX_RUNS];
    private int runsLength = 0;
    private int stackSize = 0;

    public int getMinRun(int n) {
        int r = 0;
        while (n >= 64) {
            r |= n & 1;
            n >>= 1;
        }
        return n + r;
    }

    public void printMinRuns() {
        int aLength = 0;
        for (int i = 0; i < runsLength; i++) {
            for (int k = 0; k < runsSizes[i]; k++) {
                System.out.print(a[runsIndexes[i] + k]);
                if (k != runsSizes[i] - 1) {
                    System.out.print(", ");
                }
            }
            aLength += runsSizes[i];
            System.out.println();
        }
        System.out.println("TOTAL NUMBER OF ELEMENTS: " + String.valueOf(aLength));
    }



    public void reverse(int[] a, int l, int r) {
        while (l < r) {
            int tmp = a[l];
            a[l] = a[r];
            a[r] = tmp;
            r--;
            l++;
        }
    }

    /**
     * a[l..i) - sorted subarray. This method inserts a[i] in the right place so that a[l..i] is sorted.
     *
     * @param a
     * @param l
     * @param i
     */
    public void insertWithSort(int[] a, int l, int i) {
        while (i > l && a[i] < a[i - 1]) {
            int tmp = a[i];
            a[i] = a[i - 1];
            a[i - 1] = tmp;
            i--;
        }
    }

    public void mergeSort(int[] a, int l1, int r1, int l2, int r2) {
        r1--;
        r2--;
        int[] tmp = new int[r1 - l1 + 1 + r2 - l2 + 1];
        int i = 0;
        int j = 0;
        int k = 0;
        while (i < r1 - l1 + 1 && j < r2 - l2 + 1) {
            if (a[l1 + i] <= a[l2 + j]) {
                tmp[k] = a[l1 + i];
                i++;
            } else {
                tmp[k] = a[l2 + j];
                j++;
            }
            k++;
        }

        while (i < r1 - l1 + 1) {
            tmp[k++] = a[l1 + i];
            i++;
        }

        while (j < r2 - l2 + 1) {
            tmp[k++] = a[l2 + j];
            j++;
        }

        for (int l = 0; l < k; l++) {
            a[l1 + l] = tmp[l];
        }
    }

    public void timSort(int[] a) {
        this.a = a;
        int minRunLength = getMinRun(a.length);
        int i = 0;
        int currRunStart = 0;
        while (i < a.length) {
            if (i + 1 < a.length && a[i + 1] >= a[i]) {
                while (i + 1 < a.length && a[i + 1] >= a[i]) {
                    i++;
                }
            } else {
                while (i + 1 < a.length && a[i + 1] < a[i]) {
                    i++;
                }
                reverse(a, currRunStart, i);
            }
            while (i + 1 < a.length && i - currRunStart + 1 < minRunLength) {
                i++;
                insertWithSort(a, currRunStart, i);
            }
            runsSizes[runsLength] = i - currRunStart + 1;
            runsIndexes[runsLength] = currRunStart;
            runsLength++;
            currRunStart = i + 1;
            i++;
        }


        int runsI = 0;
        while (runsI < runsLength) {
            if (stackSize > 2) {
                int x = stackSizes[stackSize - 1];
                int x_i = stackIndexes[stackSize - 1];
                int y = stackSizes[stackSize - 2];
                int y_i = stackIndexes[stackSize - 2];
                int z = stackSizes[stackSize - 3];
                int z_i = stackIndexes[stackSize - 3];
                if (!(z > y + x && y > x)) {
                    if (x < z) {
                        mergeSort(a, y_i, y_i + y, x_i, x_i + x);
                        stackSizes[stackSize - 2] += x;
                    } else {
                        mergeSort(a, z_i, z_i + z, y_i, y_i + y);
                        stackSizes[stackSize - 2] = x;
                        stackIndexes[stackSize - 2] = x_i;
                        stackSizes[stackSize - 3] += y;
                    }
                    stackSize--;
                    continue;
                }
            }
            // get next run
            stackIndexes[stackSize] = runsIndexes[runsI];
            stackSizes[stackSize] = runsSizes[runsI];
            runsI++;
            stackSize++;
        }

        while (stackSize > 1) {
            int x = stackSizes[stackSize - 1];
            int x_i = stackIndexes[stackSize - 1];
            int y = stackSizes[stackSize - 2];
            int y_i = stackIndexes[stackSize - 2];
            mergeSort(a, y_i, y_i + y, x_i, x_i + x);
            stackSize--;
            stackSizes[stackSize - 1] += x;
        }
    }



}
