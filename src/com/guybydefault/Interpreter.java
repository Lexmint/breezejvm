package com.guybydefault;

import com.guybydefault.parser.BinaryModule;
import com.guybydefault.parser.Section;
import com.guybydefault.parser.Symbol;

import java.util.Scanner;

/**
 * Created: 02/12/17 (14:44).
 */
public class Interpreter {

    private MemoryRegion data;
    private MemoryRegion code;
    private MemoryStack stack;

    private int codeCursor;

    private BinaryModule bm;

    public static final int INTEGER_SIZE = 4;

    private final static String DATA_SECTION_NAME = "_data";
    private final static String CODE_SECTION_NAME = "_code";

    private int getVariableValueByLabel(String label) {
        return data.readInt(bm.getOffsetByLabel(label));
    }

    Interpreter(BinaryModule bm) {
        this.bm = bm;

        Section dataSection = bm.getSectionByName(DATA_SECTION_NAME);
        Section codeSection = bm.getSectionByName(CODE_SECTION_NAME);

        this.data = new MemoryRegion(bm.getBlobs()[dataSection.getBlobIndex()].clone());
        int maxStackLength = getVariableValueByLabel("stackLength");
        if (data.getSize() < maxStackLength) {
            throw new IllegalArgumentException("Data length < maxStackLength.");
        }
        stack = new MemoryStack(this.data, 0, maxStackLength);
        code = new MemoryRegion(bm.getBlobs()[codeSection.getBlobIndex()].clone());
    }

    private void conditionalJumpInstruction(Instruction instruction) {
        int off = nextIntFromCode();
        boolean condition;
        switch (instruction) {
            case ifeq:
                condition = stack.pop() == 0;
                break;
            case ifne:
                condition = stack.pop() != 0;
                break;
            case iflt:
                condition = stack.pop() < 0;
                break;
            case ifge:
                condition = stack.pop() >= 0;
                break;
            case ifgt:
                condition = stack.pop() > 0;
                break;
            case ifle:
                condition = stack.pop() <= 0;
                break;
            case ificmpeq:
                condition = stack.pop() == stack.pop();
                break;
            case ificmpne:
                condition = stack.pop() != stack.pop();
                break;
            case ificmplt:
                condition = stack.pop() > stack.pop();
                break;
            case ificmpgt:
                condition = stack.pop() < stack.pop();
                break;
            case ificmpge:
                condition = stack.pop() <= stack.pop();
                break;
            case ificmple:
                condition = stack.pop() >= stack.pop();
                break;
            default:
                throw new UnsupportedOperationException("Unknown instruction " + code.getByte(codeCursor - 1));
        }
        if (condition) {
            codeCursor = off;
        }
    }

    /**
     * @param input              Input array (ints from this array will be loaded with instruction iread when requested),
     *                           can be null if there's no input
     * @param debugMode          If true - each instruction and closest label to this instruction
     *                           will be displayed, and also stack state after executing each
     *                           instruction will be printed.
     * @param variablesMonitored Labels of the variables which values will be showed after executing each instruction
     * @param arraysMonitored    Labels of arrays which content will be showed after executing each instruction
     */
    public void execute(int[] input, boolean debugMode, String[] variablesMonitored, String[] arraysMonitored) {
        int currInputIndex = 0;
        codeCursor = (int) bm.getModuleHeader().getEntryPoint();
        Scanner sc = new Scanner(System.in);
        while (codeCursor < code.getSize()) {
            Instruction ins = code.readInstruction(codeCursor++);

            if (debugMode) {
                System.out.println(ins.toString());
            }

            switch (ins.getType()) {
                case CONDITIONAL_JUMP:
                    conditionalJumpInstruction(ins);
                    break;
                case OTHER:
                    switch (ins) {
                        case nop:
                            break;
                        case ipush:
                            stack.push(nextIntFromCode());
                            break;
                        case mipush:
                            stack.push(data.readInt(nextIntFromCode()));
                            break;
                        case iaload:
                            int index = stack.pop();
                            int address = stack.pop();
                            stack.push(data.readInt(address + index * INTEGER_SIZE));
                            break;
                        case istore:
                            int offset = nextIntFromCode();
                            data.writeIntToMemory(offset, stack.pop());
                            break;
                        case iastore:
                            int value = stack.pop();
                            index = stack.pop();
                            address = stack.pop();
                            data.writeIntToMemory(address + index * INTEGER_SIZE, value);
                            break;
                        case pop:
                            stack.pop();
                            break;
                        case dup:
                            stack.push(stack.peek());
                        case dup2:
                            int val2 = stack.pop();
                            int val1 = stack.pop();
                            stack.push(val1);
                            stack.push(val2);
                            stack.push(val1);
                            stack.push(val2);
                            break;
                        case iadd:
                            stack.push(stack.pop() + stack.pop());
                            break;
                        case isub:
                            stack.push(-stack.pop() + stack.pop());
                            break;
                        case imul:
                            stack.push(stack.pop() * stack.pop());
                            break;
                        case idiv:
                            stack.push(stack.pop() / stack.pop());
                            break;
                        case irem:
                            stack.push(stack.pop() % stack.pop());
                            break;
                        case ineg:
                            stack.push(-stack.pop());
                            break;
                        case ishl:
                            int shift = stack.pop();
                            stack.push(stack.pop() << shift);
                            break;
                        case ishr:
                            shift = stack.pop();
                            stack.push(stack.pop() >> shift);
                            break;
                        case iushr:
                            shift = stack.pop();
                            stack.push(stack.pop() >>> shift);
                            break;
                        case iand:
                            stack.push(stack.pop() & stack.pop());
                            break;
                        case ior:
                            stack.push(stack.pop() | stack.pop());
                            break;
                        case ixor:
                            stack.push(stack.pop() ^ stack.pop());
                            break;
                        case idec:
                            stack.push(stack.pop() - 1);
                            break;
                        case iinc:
                            stack.push(stack.pop() + 1);
                            break;
                        case jmp:
                            codeCursor = nextIntFromCode();
                            break;
                        case invoke:
                            int argsNumber = stack.pop();
                            int moveTo = nextIntFromCode();
                            stack.push(codeCursor);
                            stack.reverseLastElements(argsNumber + 1);
                            codeCursor = moveTo;
                            break;
                        case ret:
                            codeCursor = stack.pop();
                            break;
                        case ireturn:
                            int res = stack.pop();
                            codeCursor = stack.pop();
                            stack.push(res);
                            break;
                        case iread:
                            if (input != null) {
                                stack.push(input[currInputIndex++]);
                            } else {
                                stack.push(sc.nextInt());
                            }
                            break;
                        case iwrite:
                            System.out.println("iwrite: " + stack.pop());
                            break;
                        default:
                            throw new UnsupportedOperationException("Unknown instruction " + code.getByte(codeCursor - 1));
                    }
                    break;
            }

            if (debugMode) {
                System.out.println("Current Label: " + getLabelNameClosestToCodeOffset(codeCursor));
                System.out.println("Stack: " + stack);
            }

            for (String label : arraysMonitored) {
                System.out.print(label + ": ");
                data.printIntArray(bm.getOffsetByLabel(label) + INTEGER_SIZE, getVariableValueByLabel(label));
            }

            for (String label : variablesMonitored) {
                System.out.print(label + ": ");
                System.out.println(getVariableValueByLabel(label));
            }

            if (variablesMonitored.length != 0 || arraysMonitored.length != 0 || debugMode) {
                System.out.println();
            }
        }
    }

    public int[] getArrayFromDataByLabel(String arrayLabel, int numOfElements) {
        return data.readArray(bm.getOffsetByLabel(arrayLabel), numOfElements);
    }

    public int getIntFromDataByLabel(String label) {
        return data.readInt(bm.getOffsetByLabel(label));
    }

    private String getLabelNameClosestToCodeOffset(int codeOffset) {
        int codeSectionIndex = bm.getSectionByName(CODE_SECTION_NAME).getSectionId();
        Symbol res = null;
        for (Symbol symbol : bm.getSymbols()) {
            if (symbol.getSectionIndex() == codeSectionIndex) {
                if (codeOffset > symbol.getBlobEntryIndex() && (res == null || res.getBlobEntryIndex() < symbol.getBlobEntryIndex())) {
                    res = symbol;
                }
            }
        }
        return res == null ? null : res.getName();
    }


    private int nextIntFromCode() {
        int res = code.readInt(codeCursor);
        codeCursor += Interpreter.INTEGER_SIZE;
        return res;
    }
}
