package com.guybydefault;

import com.guybydefault.parser.BinaryModule;

import java.io.IOException;
import java.util.Arrays;
import java.util.Random;

public class Launcher {

    private static void testTimSort(String listingFileName, boolean debugMode) throws IOException {
        BinaryModule bm = BinaryModule.importModule(listingFileName);
        Interpreter interpreter = new Interpreter(bm);

        Random random = new Random();
        for (int i = 0; i < 500; i++) {

            /* Generating array */
            int notSortedArray[] = new int[i];
            for (int k = 0; k < i; k++) {
                notSortedArray[k] = random.nextInt(30);
            }

            /* Sorting using interpreter & TimSort ASM listing */
            int[] timSortSorted = new int[i + 1];
            timSortSorted[0] = i; // length of the input array for interpreter must be just before the array
            System.arraycopy(notSortedArray, 0, timSortSorted, 1, i);

            if (debugMode) {
                interpreter.execute(timSortSorted, true, new String[]{
                        "runsLength",
                        "arrayLength",
                        "minRunLength",
                        "runsLength"
                }, new String[]{"arrayLength"});
            } else {
                interpreter.execute(timSortSorted, false, new String[]{}, new String[]{});
            }
            timSortSorted = interpreter.getArrayFromDataByLabel("array", i);

            int[] javaSortedArray = notSortedArray.clone();
            Arrays.sort(javaSortedArray);

            boolean isActuallySorted = Arrays.equals(javaSortedArray, timSortSorted);
            if (!isActuallySorted) {
                System.out.println("TIMSORT IS NOT WORKING CORRECTLY!");
                System.out.println("Array length " + i);
                System.out.println("Timsorted   : " + Arrays.toString(timSortSorted));
                System.out.println("Java Sorted : " + Arrays.toString(javaSortedArray));
                System.out.println("SourceArray : " + Arrays.toString(notSortedArray));
                break;
            }

            if (i % 100 == 0) {
                System.out.println("Array of length " + i + " tested SUCCESSFULLY!");
            }
        }
    }

    private static void testFib(String listingFileName) throws IOException {
        BinaryModule bm = BinaryModule.importModule(listingFileName);

        for (int i = 0; i < 25; i++) {
            Interpreter interpreter = new Interpreter(bm);
            int[] input = new int[1];
            input[0] = i;
            interpreter.execute(input, true, new String[]{"n", "i"}, new String[]{});
            int result = interpreter.getIntFromDataByLabel("a"); // result of fib alg is stored in 'a'
            System.out.println("Fib(" + i + ") = " + result);
        }
    }

    public static void main(String[] args) throws IOException {
        testTimSort("/Users/lexmint/Desktop/Timsort.ptptb", false);
        testFib("/Users/lexmint/Desktop/Fib.ptptb");
    }
}
